%% EE242 Project
% 
% * Authors:    John Lipor, Carlos Tejada, Jose Solares
% * Submitted:  27/11/11
%

clear

% Generate 100,000 BPSK symbols
symbols = randsrc(100000,1,[1 -1; 0.5 0.5]);

% Channel
h_ch = [1 0.5];

% Sigma sy vectors
sig_sy_0 = [1 0 0];
sig_sy_1 = [0.5 1 0];

% SNR in dB
SNR_vector = linspace(0,12,100);

% Analytical MSE
mse_0 = zeros(length(SNR_vector),1);
mse_1 = zeros(length(SNR_vector),1);

% Estimated MSE
mse_est_0 = zeros(length(SNR_vector),1);
mse_est_1 = zeros(length(SNR_vector),1);

% BERs
ber_0 = zeros(length(SNR_vector),1);
ber_1 = zeros(length(SNR_vector),1);

for i = 1:length(SNR_vector);
    
    % Linear SNR
    SNR = 10^(SNR_vector(i)/10);
    sig = 1.25/SNR;
    
    % Create y = s(i) + 0.5*s(i-1) + v(i)
    y = filter(h_ch,1,symbols) + sqrt(sig)*randn(100000,1);
    
    % LMMSE equalizer coefficients
    sig_yy = [1.25+sig^2 0.5 0;0.5 1.25+sig^2 0.5;0 0.5 1.25+sig^2];
    coefficients_0 = sig_sy_0/sig_yy;   % sig_sy*(sig_yy)^-1
    coefficients_1 = sig_sy_1/sig_yy;   % sig_sy*(sig_yy)^-1
    
    % Put y through equalizer
    % delta = 0
    g_0 = filter(coefficients_0,1,y);
    % delta = 1
    g_1 = filter(coefficients_1,1,y);
    
    % Calculate MSEs
    % delta = 0
    mse_0(i) = 1 - sig_sy_0/sig_yy*sig_sy_0';
    mse_est_0(i) = (1/(100000-1))*sum(abs(symbols(3:end) - g_0(3:end)).^2);
    % delta = 1
    mse_1(i) = 1 - sig_sy_1/sig_yy*sig_sy_1';
    mse_est_1(i) = (1/(100000-1-1))*sum(abs(symbols(3:end-1) - ...
        g_1(4:end)).^2);
    
    % Decision device
    g_0(g_0<0) = -1;
    g_0(g_0>0) = 1;
    g_1(g_1<0) = -1;
    g_1(g_1>0) = 1;
    
    % Calculate BERs
    ber_0(i) = sum(g_0~=symbols)/100000;
    ber_1(i) = sum(g_1(2:end)~=symbols(1:end-1))/100000;
    
end

% Plot 1
figure
plot(SNR_vector,mse_0,SNR_vector,mse_1)
title('Analytical MSE vs. SNR')
xlabel('SNR (dB)')
ylabel('mean-squared error')
legend('\Delta = 0','\Delta = 1')
grid on

% Plot 2
figure
plot(SNR_vector,mse_0,SNR_vector,mse_est_0)
title('Analytical and Estimated MSE vs. SNR, \Delta = 0')
xlabel('SNR (dB)')
ylabel('mean-squared error')
legend('analytical','estimated')
grid on

% Plot 3
figure
plot(SNR_vector,mse_1,SNR_vector,mse_est_1)
title('Analytical and Estimated MSE vs. SNR, \Delta = 1')
xlabel('SNR (dB)')
ylabel('mean-squared error')
legend('analytical','estimated')
grid on

% Plot 4
figure
semilogy(SNR_vector,ber_0,SNR_vector,ber_1)
title('BER vs. SNR')
xlabel('SNR (dB)')
ylabel('mean-squared error')
legend('\Delta = 0','\Delta = 1')
grid on